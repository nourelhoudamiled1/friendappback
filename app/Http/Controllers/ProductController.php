<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Repositories\ProductRepository;
use App\Services\FileService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * @OA\Tag(name="Product")
 */
class ProductController extends ApiController
{

    private ProductRepository $productRepository;
    private ProductService $productService;
    private FileService $fileService;

    /**
     * @param ProductRepository  $productRepository
     * @param ProductService $productService
     * @param FileService $fileService

     */
    public function __construct(
        ProductRepository $productRepository,
        ProductService $productService,
        FileService $fileService

    ) {
        $this->productRepository = $productRepository;
        $this->productService = $productService;
        $this->fileService = $fileService;
    }

    /**
     * @OA\Get(
     *      path="/products",
     *      operationId="getproductList",
     *      tags={"Product"},
     * security={
     *  {"passport": {}},
     *   },
     *      summary="get list of products",
     *      description="Returns  list of products",
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                @OA\Property(property="product_name", type="string"),
     *                @OA\Property(property="long_description", type="string", nullable=true),
     *                @OA\Property(property="short_description", type="string", nullable=true),
     *                @OA\Property(property="file_original", type="file", nullable=true),
     *                @OA\Property(property="file_zip", type="file", nullable=true),
     *                @OA\Property(property="file_related", type="array", @OA\Items()),
     *                @OA\Property(property="category_id", type="integer", nullable=true),
     *                @OA\Property(property="product_id", type="integer"),
     *                @OA\Property(property="enableDisable", type="integer", nullable=true)
     *             )
     *         )
     *     ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */

    public function products()
    {
        $products = $this->productRepository->products();
        return $this->successResponse($products);
    }

    /**
     * @OA\Post(
     *     path="/saveProduct",
     *     tags={"Product"},
     *     security={{"passport": {}}},
     *     summary="Save a product",
     *     operationId="saveProduct",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(property="product_name", type="string"),
     *                 @OA\Property(property="long_description", type="string", nullable=true),
     *                 @OA\Property(property="short_description", type="string", nullable=true),
     *                 @OA\Property(property="file_original", type="file", nullable=true, description="Accepts image files"),
     *                 @OA\Property(property="file_zip", type="file", nullable=true, description="Accepts .zip files only"),
     *                 @OA\Property(
     *                      property="file_related[file]",
     *                      type="array",
     *                      description="Accepts image files",
     *                      @OA\Items(
     *                          type="file",
     *                          @OA\Property(property="file_zip", type="file", nullable=true, description="Accepts .zip files only"),
     *                      ),
     *               ),
     *               @OA\Property(property="category_id", type="integer", nullable=true),
     *               @OA\Property(property="product_id", type="integer", nullable=true),
     *               @OA\Property(property="enableDisable", type="integer", enum={0, 1}, nullable=true)
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", ref="#/components/schemas/ProductResource")
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(property="errors", type="object")
     *         )
     *     )
     * )
     *
     * @OA\Schema(
     *     schema="ProductResource",
     *     @OA\Property(property="product_name", type="string"),
     *     @OA\Property(property="long_description", type="string", nullable=true),
     *     @OA\Property(property="short_description", type="string", nullable=true),
     *     @OA\Property(property="file_original", type="file", nullable=true),
     *     @OA\Property(property="file_zip", type="file", nullable=true),
     *     @OA\Property(property="file_related", type="array", @OA\Items()),
     *     @OA\Property(property="category_id", type="integer", nullable=true),
     *     @OA\Property(property="product_id", type="integer", nullable=true),
     *     @OA\Property(property="enableDisable", type="integer", nullable=true)
     * )
     */
    public function saveProduct(Request $request)
    {
        $rules = [
            'product_id' => 'nullable|exists:product,product_id',
            'category_id' => 'nullable|exists:category,category_id',
            'product_name' => 'required',
            'long_description' => 'required',
            'short_description' => 'required',
            'file_related' => 'required|array',
            'file_related.*.file' => 'required|mimes:jpeg,png,jpg,gif,svg',
            'file_zip' => 'nullable',
            'file_original' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
            'enableDisable' => 'nullable|in:' . implode(',', [0, 1]),
        ];
        $errors = $this->validateResponse($request->all(), $rules);
        if (!empty($errors)) {
            return $this->errorResponse($errors, 422);
        }
        $enableDisable = null;
        if ($request->has('enableDisable')) {
            $enableDisable = $request->enableDisable;
        }
        $longDescription = null;
        if ($request->has('long_description')) {
            $longDescription = $request->long_description;
        }
        $shortDescription = null;
        if ($request->has('short_description')) {
            $shortDescription = $request->short_description;
        }
        $productId = null;
        if ($request->has('product_id')) {
            $productId = $request->product_id;
        }
        $categoryId = null;
        if ($request->has('category_id')) {
            $categoryId = $request->category_id;
        }

        //file related
        $fileRelatedIds = [];
        if ($request->has('file_related')) {
            $file_related = $request->file('file_related');
            foreach ($file_related as $item) {
                $file = $this->fileService->fileMap($item);
                $fileRelatedIds[] = $file->file_id;
            }
        }

        //file zip
        $fileZipId = null;
        if ($request->hasFile('file_zip')) {
            $file_zip = $request->file('file_zip');
            $file = $this->fileService->fileMap($file_zip);
            $fileZipId = $file->file_id;
        }

        //file image original
        $fileOriginalId = null;
        if ($request->hasFile('file_original')) {
            $file_original =$request->file('file_original');
            $file = $this->fileService->fileMap($file_original);
            $fileOriginalId = $file->file_id;
        }
        $product = $this->productService->saveProduct(
            $productId,
            $request->product_name,
            $categoryId,
            $longDescription,
            $shortDescription,
            $fileRelatedIds,
            $fileZipId,
            $fileOriginalId,
            $enableDisable
        );
        return $this->successResponse(new ProductResource($product));
    }
}
