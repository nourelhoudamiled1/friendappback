<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryResource;
use App\Repositories\CategoryRepository;
use App\Services\CategoryService;
use Illuminate\Http\Request;

/**
 * @OA\Tag(name="Category")
 */
class CategoryController extends ApiController
{

    private CategoryRepository $categoryRepository;
    private CategoryService $categoryService;
    /**
     * @param CategoryRepository  $categoryRepository
     * @param CategoryService $categoryService
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        CategoryService $categoryService
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->categoryService = $categoryService;
    }

    /**
     * @OA\Get(
     *      path="/categories",
     *      operationId="getcategoryList",
     *      tags={"Category"},
     * security={
     *  {"passport": {}},
     *   },
     *      summary="get list of categories",
     *      description="Returns  list of categories",
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 @OA\Property(property="category_id", type="integer"),
     *                 @OA\Property(property="category_name", type="string"),
     *                 @OA\Property(property="description", type="string", nullable=true),
     *                 @OA\Property(property="enableDisable", type="integer", nullable=true),
     *             )
     *         )
     *     ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */

    public function categories()
    {
        $categories = $this->categoryRepository->categories();
        return $this->successResponse($categories);
    }
    /**
     * @OA\Post(
     *     path="/saveCategory",
     *     tags={"Category"},
     *     security={
     *  {"passport": {}},
     *   },
     *     summary="Save a category",
     *     operationId="saveCategory",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="category_name", type="string"),
     *             @OA\Property(property="description", type="string", nullable=true),
     *             @OA\Property(property="category_id", type="integer", nullable=true),
     *             @OA\Property(property="enableDisable", type="integer", enum={0, 1}, nullable=true),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", ref="#/components/schemas/CategoryResource")
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(property="errors", type="object")
     *         )
     *     )
     * )
     *
     * @OA\Schema(
     *     schema="CategoryResource",
     *     @OA\Property(property="category_id", type="integer"),
     *     @OA\Property(property="category_name", type="string"),
     *     @OA\Property(property="description", type="string", nullable=true),
     *     @OA\Property(property="enableDisable", type="integer", nullable=true),
     * )
     */

    public function saveCategory(Request $request)
    {
        $rules = [
            'description' => 'nullable',
            'category_id' => 'nullable|exists:category,category_id',
            'category_name' => 'required',
            'enableDisable' => 'nullable|in:' . implode(',', [0, 1]),
        ];
        $errors = $this->validateResponse($request->all(), $rules);
        if (!empty($errors)) {
            return $this->errorResponse($errors, 422);
        }
        $enableDisable = null;
        if ($request->has('enableDisable')) {
            $enableDisable = $request->enableDisable;
        }
        $description = null;
        if ($request->has('description')) {
            $description = $request->description;
        }
        $categoryId = null;
        if ($request->has('category_id')) {
            $categoryId = $request->category_id;
        }
        $category = $this->categoryService->saveCategory(
            $categoryId,
            $request->category_name,
            $description,
            $enableDisable
        );
        return $this->successResponse(new CategoryResource($category));
    }
}
