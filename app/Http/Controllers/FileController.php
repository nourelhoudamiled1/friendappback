<?php

namespace App\Http\Controllers;

use App\Repositories\FileRepository;
use App\Services\FileService;
use Illuminate\Http\Request;

/**
 * @OA\Tag(name="File")
 */
class FileController extends ApiController
{

    private FileRepository $fileRepository;
    private FileService $fileService;
    /**
     * @param FileRepository  $fileRepository
     * @param FileService $fileService
     */
    public function __construct(
        FileRepository $fileRepository,
        FileService $fileService
    ) {
        $this->fileRepository = $fileRepository;
        $this->fileService = $fileService;
    }

    /**
     * @OA\Get(
     *      path="/files",
     *      operationId="getfileList",
     *      tags={"File"},
     * security={
     *  {"passport": {}},
     *   },
     *      summary="get list of files",
     *      description="Returns  list of files",
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 @OA\Property(property="file_id", type="integer"),
     *                 @OA\Property(property="file_name", type="string"),
     *                 @OA\Property(property="description", type="string", nullable=true),
     *                 @OA\Property(property="enableDisable", type="integer", nullable=true),
     *             )
     *         )
     *     ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */

    public function files()
    {
        $files = $this->fileRepository->files();
        return $this->successResponse($files);
    }
}
