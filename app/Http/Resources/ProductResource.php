<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $category = $this->category;
        $file_original = $this->file_original;
        $file_zip = $this->file_zip;
        $file_related = $this->file_related;
        return [
            'product_id' => $this->product_id,
            'product_name' => $this->product_name,
            'long_description' => $this->long_description,
            'short_description' => $this->short_description,
            'file_original_id' => $this->file_original_id,
            'file_zip_id' => $this->file_zip_id,
            'file_related' => $file_related,
            'category_id' => $this->category_id,
            'category_name' => $category ? $category->category_name  : null,
            'file_image_name' => $file_original ? $file_original->file_name  : null,
            'category_name' => $category ? $category->category_name  : null,
            'file_zip_name' => $file_zip ? $file_zip->file_name  : null,
            'deleted_at' => $this->deleted_at
        ];
    }
}
