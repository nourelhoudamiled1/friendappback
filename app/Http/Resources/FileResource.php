<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'file_id' => $this->file_id,
            'file_name' => $this->file_name,
            'description' => $this->description,
            'extension' => $this->extension,
            'size' => $this->size,
            'path' => $this->path,
            'url' => $this->url,
            'type' => $this->type,
            'deleted_at' => $this->deleted_at
        ];
    }
}
