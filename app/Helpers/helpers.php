<?php
if (! function_exists('validateJson')) {
function validateJson($value)
{
if (! is_scalar($value) && ! method_exists($value, '__toString')) {
return false;
}

json_decode($value);

return json_last_error() === JSON_ERROR_NONE;
}
}
