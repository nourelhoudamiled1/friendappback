<?php

namespace App\Traits;

use Illuminate\Support\Facades\Validator;

trait ApiResponser
{

    protected function successResponse($data, $code = 200)
    {
        return response()->json($data, $code)->setStatusCode($code);
    }
    protected function errorResponse($message, $code)
    {
        $messages = [];

        if (validateJson($message)) {
            $message = json_decode($message, true);
        }

        if (is_array($message)) {
            foreach ($message as $field => $error_bag) {
                array_push($messages, $error_bag[0]);
            }
        } else {
            array_push($messages, $message);
        }

        if (empty($code)) {
            $code = 500;
        }

        return response()->json(['error' => ["messages" => $messages], 'code' => $code], $code);
    }
    /**
     * @param $data
     * @param $rules
     * @return array|MessageBag
     */
    protected function validateResponse($data, $rules)
    {
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return $validator->errors();
        }
        return [];
    }
}
