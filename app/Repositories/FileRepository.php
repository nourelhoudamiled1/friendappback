<?php

namespace App\Repositories;

use App\Models\File;

/**
 * Class FileRepository
 * @package App\Repository
 */
class FileRepository
{
    /**
     * Get the files.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function files()
    {
        /** @var \Illuminate\Database\Eloquent\Builder $data */
        $data = File::withTrashed();

        $data = $data->select([
            'file.file_name',
            'file.file_id',
            'file.deleted_at',
            'file.description',
            'file.size',
            'file.extension',
            'file.path',
            'file.url',
            'file.type'
        ])->orderByDesc('file.file_id')
            ->get();

        return $data;
    }
}
