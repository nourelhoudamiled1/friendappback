<?php

namespace App\Repositories;

use App\Models\Category;

/**
 * Class CategoryRepository
 * @package App\Repository
 */
class CategoryRepository
{
    /**
     * Get the categories.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function categories()
    {
        /** @var \Illuminate\Database\Eloquent\Builder $data */
        $data = Category::withTrashed();

        $data = $data->select([
            'category.category_name',
            'category.category_id',
            'category.deleted_at',
            'category.description'
        ])->orderByDesc('category.category_id')
            ->get();

        return $data;
    }
}
