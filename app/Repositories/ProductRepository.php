<?php

namespace App\Repositories;

use App\Models\Product;

/**
 * Class ProductRepository
 * @package App\Repository
 */
class ProductRepository
{
    /**
     * Get the products.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function products()
    {

        /** @var \Illuminate\Database\Eloquent\Builder $data */
        $data = Product::withTrashed();

        $data = $data->select([
            'product.product_name',
            'product.product_id',
            'product.deleted_at',
            'product.long_description',
            'product.short_description',
            'product.file_original_id',
            'product.file_zip_id',
            'image_origin.file_name as file_image_name',
            'zip.file_name as file_zip_name',
            'product.category_id',
            'category.category_name'
        ])->leftJoin('file as zip', 'zip.file_id', '=', 'product.file_zip_id')
            ->leftJoin('file as image_origin', 'image_origin.file_id', '=', 'product.file_original_id')
            ->leftJoin('category', 'category.category_id', '=', 'product.category_id')
            ->file_related()
        ->orderByDesc('product.product_id')
            ->get();

        return $data;
    }
}
