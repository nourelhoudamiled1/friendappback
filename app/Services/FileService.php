<?php

namespace App\Services;

use App\Models\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class FileService
 * @package App\Services
 */
class FileService
{

    /**
     * save File.
     * @param  int|null  $fileId  The ID of the File to update. Defaults to null, meaning create a new File.
     * @param  string  $name  The name of the File.
     * @param  string  $description  The description of the File.
     * @param  string  $size  The size of the File.
     * @param  string  $extension  The extension of the File.
     * @param  string  $path  The path of the File.
     * @param string  $url  The url of the File.
     * @param  string  $type  The type of the File.
     * @param  int|null  $enableDisable  Whether to enable (1) or disable (0) the File. Defaults to null, meaning no action is taken.
     * @return File The saved File.
     */

    public function saveFile(
        $fileId = null,
        $name,
        $description,
        $size,
        $extension,
        $path,
        $url,
        $type,
        $enableDisable = null
    ): File {
        $file = File::withTrashed()->find($fileId);
        $userConnected = Auth::user()->user_id;
        if (!$fileId) { //create File
            $fileNew = new File();
            $fileNew->file_name = $name;
            $fileNew->description = $description;
            $fileNew->extension = $extension;
            $fileNew->size = $size;
            $fileNew->path = $path;
            $fileNew->url = $url;
            $fileNew->type = $type;
            $fileNew->created_by = $userConnected;
            $fileNew->save();
            $file = $fileNew;
        } else { // update/delete file
            $file->file_name = $name;
            $file->description = $description;
            $file->size = $size;
            $file->extension = $extension;
            $file->path = $path;
            $file->url = $url;
            $file->type = $type;
            $file->updated_by = $userConnected;
            if (!is_null($enableDisable)) {
                $enableDisable == 0 ? $file->restore() && $file->updated_by = null : $file->delete() && $file->updated_by = $userConnected;
            }
            $file->save();
        }
        return $file;
    }
    /**
     * Process and save a file.
     *
     * @param array $file The file to be processed and saved.
     * @return File The saved file object.
     */
    public function fileMap($filez)
    {
        $filename = null;
        $size = null;
        $extension = null;
        $path = null;
        $url = null;

        $filename = str_replace(' ', '', $filez->getClientOriginalName());
        $size = $filez->getSize();
        $random_number = rand();
        $file_names = explode(".", $filename);
        $path_title = $file_names[0] . '_' . $random_number . '.' . $file_names[1];
        $path = $filez->move(public_path('uploads'), $path_title);
        $url = url('/uploads/' . $path_title);
        $extension = $filez->getClientOriginalExtension();
        $file = $this->saveFile(
            null,
            $filename,
            null,
            $size,
            $extension,
            $path,
            $url,
            'image'
        );
        return $file;
    }
}
