<?php

namespace App\Services;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class CategoryService
 * @package App\Services
 */
class CategoryService
{

    /**
     * save Category.
     * @param  int|null  $categoryId  The ID of the Category to update. Defaults to null, meaning create a new Category.
     * @param  string  $name  The name of the Category.
     * @param  int|string  $description  The description of the Category.
     * @param  int|null  $enableDisable  Whether to enable (1) or disable (0) the Category. Defaults to null, meaning no action is taken.
     * @return Category The saved Category.
     */
    public function saveCategory(
        $categoryId = null,
        $name,
        $description,
        $enableDisable = null
    ): Category {
        $category = Category::withTrashed()->find($categoryId);
        $userConnected = Auth::user()->user_id;
        if (!$categoryId) { //create Category
            $categoryNew = new Category();
            $categoryNew->category_name = $name;
            $categoryNew->description = $description;
            $categoryNew->created_by = $userConnected;
            $categoryNew->save();
            $category = $categoryNew;
        } else { // update/delete category
            $category->category_name = $name;
            $category->description = $description;
            $category->updated_by = $userConnected;
            if (!is_null($enableDisable)) {
                $enableDisable == 0 ? $category->restore() && $category->updated_by = null : $category->delete() && $category->updated_by = $userConnected;
            }
            $category->save();
        }
        return $category;
    }
}
