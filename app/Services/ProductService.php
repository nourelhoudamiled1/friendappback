<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class ProductService
 * @package App\Services
 */
class ProductService
{
    /**
     * save Product.
     * @param  int|null  $productId  The ID of the Product to update. Defaults to null, meaning create a new Product.
     * @param  string  $name  The name of the Product.
     * @param  int  $categoryId  The categoryId of the Product.
     * @param  string  $longDescription  The longDescription of the Product.
     * @param  string  $shortDescription  The shortDescription of the Product.
     * @param  array  $fileRelatedIds  The related file array ids of the Product.
     * @param  int  $fileZipId  The fileZipId of the Product.
     * @param int  $fileOriginalId  The fileOriginalId of the Product.
     * @param  int|null  $enableDisable  Whether to enable (1) or disable (0) the Product. Defaults to null, meaning no action is taken.
     * @return Product The saved Product.
     */

    public function saveProduct(
        $productId = null,
        $name,
        $categoryId,
        $longDescription,
        $shortDescription,
        $fileRelatedIds,
        $fileZipId,
        $fileOriginalId,
        $enableDisable = null
    ): Product {
        $product = Product::withTrashed()->find($productId);
        $userConnected = 1;
        Log::info($userConnected);
        if (!$productId) { //create Product
            $productNew = new Product();
            $productNew->product_name = $name;
            $productNew->category_id = $categoryId;
            $productNew->long_description = $longDescription;
            $productNew->short_description = $shortDescription;
            $productNew->file_original_id = $fileOriginalId;
            $productNew->file_zip_id = $fileZipId;
            $productNew->created_by = $userConnected;
            $productNew->save();
            $product = $productNew;
        } else { // update/delete product
            $product->product_name = $name;
            $product->category_id = $categoryId;
            $product->long_description = $longDescription;
            $product->short_description = $shortDescription;
            $product->file_original_id = $fileOriginalId;
            $product->file_zip_id = $fileZipId;
            $product->updated_by = $userConnected;
            if (!is_null($enableDisable)) {
                $enableDisable == 0 ? $product->restore() && $product->updated_by = null : $product->delete() && $product->updated_by = $userConnected;
            }
            $product->save();
        }
        if ($fileRelatedIds) {
            $product->file_related()->sync($fileRelatedIds);
        }
        return $product;
    }
}
