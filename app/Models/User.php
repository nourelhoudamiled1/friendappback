<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $table = 'user';
    protected $primaryKey = 'user_id';

    protected $casts = [
        'created_by' => 'int',
        'updated_by' => 'int',
    ];

    protected $dates = [
        'pw_renew_sent_time',
        'last_seen_at'
    ];

    protected $hidden = [
        'password',
    ];

    protected $fillable = [
        'name',
        'password',
        'email',
        'default_language',
        'created_by',
        'updated_by'
    ];

}
