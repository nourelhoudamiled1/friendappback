<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'product';
    protected $primaryKey = 'product_id';
    protected $casts = [
        'created_by' => 'int',
        'updated_by' => 'int'
    ];

    protected $fillable = [
        'product_name',
        'long_description',
        'short_description',
        'file_original_id',
        'file_zip_id',
        'category_id',
        'category_id',
        'created_by',
        'updated_by',

    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function file_original()
    {
        return $this->belongsTo(File::class, 'file_original_id');
    }

    public function file_zip()
    {
        return $this->belongsTo(File::class, 'file_zip_id');
    }

    public function file_related()
    {
        return $this->belongsToMany(File::class, 'file_related_product', 'product_id', 'file_id')->withTimestamps();
    }
}
