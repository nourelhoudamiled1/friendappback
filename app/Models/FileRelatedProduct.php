<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileRelatedProduct extends Model
{
    use SoftDeletes;
    protected $table = 'file_related_product';
    protected $primaryKey = 'file_related_product_id';


    protected $fillable = [
        'file_id',
        'product_id'
    ];
}
