<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use SoftDeletes;
    protected $table = 'file';
    protected $primaryKey = 'file_id';
    protected $casts = [
        'created_by' => 'int',
        'updated_by' => 'int'
    ];

    protected $fillable = [
        'file_name',
        'description',
        'size',
        'extension',
        'path',
        'url',
        'type', //zip or image
        // 'is_uplodable', //in the future i will use
        'created_by',
        'updated_by',

    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

}
