<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//auth routes
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:api')->group( function () {

    // category
    Route::get('categories', [CategoryController::class, 'categories']);
    Route::post('saveCategory', [CategoryController::class, 'saveCategory']);

    //file
    Route::get('files', [FileController::class, 'files']);

    // product
    Route::get('products', [ProductController::class, 'products']);
    Route::post('saveProduct', [ProductController::class, 'saveProduct']);

});

