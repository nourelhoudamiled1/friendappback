<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->integer('user_id', true);
            $table->string('name', 50);
            $table->string('password');
            $table->string('email')->unique();
            $table->string('default_language')->default('fr');
            $table->integer('created_by')->index('user_created_by_foreign');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable()->index('user_updated_by_foreign');
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
            $table->foreign(['created_by'], 'fk_user_created_by')->references(['user_id'])->on('user')->onUpdate('CASCADE');
            $table->foreign(['updated_by'], 'fk_user_updated_by')->references(['user_id'])->on('user')->onUpdate('CASCADE');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
        //original
        // $table->id();
        // $table->string('name');
        // $table->string('email')->unique();
        // $table->timestamp('email_verified_at')->nullable();
        // $table->string('password');
        // $table->rememberToken();
        // $table->timestamps();
    }
};
