<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_related_product', function (Blueprint $table) {
            $table->integer('file_related_product_id', true);
            $table->integer('file_id')->index('file_related_product_file_id_foreign');
            $table->integer('product_id')->index('file_related_product_product_id_foreign');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
            $table->foreign(['product_id'], 'fk_file_related_product_product_id')->references(['product_id'])->on('product')->onUpdate('CASCADE');
            $table->foreign(['file_id'], 'fk_file_related_product_file_id')->references(['file_id'])->on('file')->onUpdate('CASCADE');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_related_product');
    }
};
