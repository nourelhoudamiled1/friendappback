<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->integer('category_id', true);
            $table->string('category_name', 50)->unique();
            $table->text('description')->nullable();
            $table->integer('created_by')->index('category_created_by_foreign');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable()->index('category_updated_by_foreign');
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
            $table->foreign(['created_by'], 'fk_category_created_by')->references(['user_id'])->on('user')->onUpdate('CASCADE');
            $table->foreign(['updated_by'], 'fk_category_updated_by')->references(['user_id'])->on('user')->onUpdate('CASCADE');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cateogry');
    }
};
