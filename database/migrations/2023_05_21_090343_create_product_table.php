<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->integer('product_id', true);
            $table->string('product_name', 500)->unique();
            $table->text('long_description')->nullable();
            $table->string("short_description")->nullable();
            $table->integer('file_original_id')->index('product_file_original_id_foreign');
            $table->integer('file_zip_id')->index('product_file_zip_id_foreign');
            $table->integer('category_id')->index('product_category_id_foreign');
            $table->integer('created_by')->index('product_created_by_foreign');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable()->index('product_updated_by_foreign');
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
            $table->foreign(['created_by'], 'fk_product_created_by')->references(['user_id'])->on('user')->onUpdate('CASCADE');
            $table->foreign(['updated_by'], 'fk_product_updated_by')->references(['user_id'])->on('user')->onUpdate('CASCADE');
            $table->foreign(['file_original_id'], 'fk_product_file_original_id')->references(['file_id'])->on('file')->onUpdate('CASCADE');
            $table->foreign(['file_zip_id'], 'fk_product_file_zip_id')->references(['file_id'])->on('file')->onUpdate('CASCADE');
            $table->foreign(['category_id'], 'fk_product_category_id')->references(['category_id'])->on('category')->onUpdate('CASCADE');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
};
