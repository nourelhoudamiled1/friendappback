<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file', function (Blueprint $table) {
            $table->integer('file_id', true);
            $table->string('file_name', 500)->unique();
            $table->text('description')->nullable();
            $table->string("extension")->nullable();
            $table->string("size")->nullable();
            $table->string("path")->nullable();
            $table->string("url")->nullable();
            $table->string("type")->nullable();
            $table->integer('created_by')->index('file_created_by_foreign');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable()->index('file_updated_by_foreign');
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
            $table->foreign(['created_by'], 'fk_file_created_by')->references(['user_id'])->on('user')->onUpdate('CASCADE');
            $table->foreign(['updated_by'], 'fk_file_updated_by')->references(['user_id'])->on('user')->onUpdate('CASCADE');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file');
    }
};
